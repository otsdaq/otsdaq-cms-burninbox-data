###################################################
#Prerequisite of packages to be installed by root
###################################################
#sudo dnf install -y libdb-devel
#sudo dnf install -y gdbm-devel
#sudo dnf install -y libfontenc-devel
#sudo dnf install -y libpciaccess libpciaccess-devel
#sudo dnf install -y libtirpc-devel
#sudo dnf install -y libXdmcp-devel libXxf86vm-devel Lmod mesa-libGLU mesa-libGLU-devel mesa-libGL-devel
#sudo dnf install -y meson nasm ninja-build patchelf python-devel python3-devel
#sudo dnf install -y texinfo texlive mesa-libGL-devel
#sudo dnf install -y xorg-x11-util-macros xorg-x11-server-devel xorg-x11-xtrans-devel
#sudo dnf install -y libxcb libxcb-devel 
#sudo dnf install -y xcb-util 
#sudo dnf install -y xcb-util-wm xcb-util-wm-devel 
#sudo dnf install -y xcb-util-keysyms xcb-util-keysyms-devel 
#sudo dnf install -y xcb-util-image xcb-util-image-devel
#sudo dnf install -y xcb-util-renderutil xcb-util-renderutil-devel
#sudo dnf install -y libxkbcommon libxkbcommon-devel libxkbcommon-x11 libxkbcommon-x11-devel  
#sudo dnf install -y libX11-xcb
#sudo dnf install -y gcc-gfortran
#sudo dnf install -y freetype freetype-devel


###################################################
#FIRST: Create and go to the directory where you want to install OTSDAQ.
#As an example, here we create the otsdaq directory and we will cd into it
###################################################
mkdir otsdaq-devel && cd otsdaq-devel
export OTSDAQ_HOME=$PWD #This will be the directory where YOU want to install OTSDAQ
export CACTUSROOT=/opt/cactus

###################################################
#Installing Spack Fermilab stuff
###################################################
cd ${OTSDAQ_HOME}
mkdir -p spack/repos
cd spack
git clone https://github.com/FNALssi/spack.git -b fnal-develop
cd spack
git reset --hard 5d8ac7656ad4b8c2850e76d4751b9e0333d80d6b
cd ..
echo 'export SPACK_DISABLE_LOCAL_CONFIG=true 
source spack/share/spack/setup-env.sh' > setup-env.sh
source setup-env.sh
git clone https://github.com/fnalssi/fermi-spack-tools.git
cd fermi-spack-tools
git reset --hard 06627a1b963fefcfc922a4789696f1e5adf27511
cd ..
./fermi-spack-tools/bin/make_packages_yaml spack

###################################################
#Create and activate ots environment
###################################################
cd ..
spack compiler find
spack env create -d .
spack env activate .

###################################################
#Installing art suite
###################################################
cd ${OTSDAQ_HOME}
cd spack/repos
#git clone https://github.com/FNALssi/fnal_art.git && spack repo add fnal_art
git clone -b eflumerf/DontUseMasterCMake https://github.com/eflumerf/fnal_art.git && spack repo add fnal_art
#sed -i 's/libxml2@2.9.12/libxml2@2.9.13/g' ${OTSDAQ_HOME}/spack/repos/fnal_art/packages/art-suite/package.py
#git clone https://github.com/uplegger/fnal_art.git && spack repo add fnal_art
cd fnal_art
git reset --hard a150e04757263b39dd7b01140c0e7b4a20ac92e1

cd ${OTSDAQ_HOME}
spack add art-suite@s126
spack concretize -f
spack install -j`nproc`

###################################################
#Installing otsdaq suite
###################################################
cd ${OTSDAQ_HOME}
cd spack/repos

git clone https://github.com/art-daq/artdaq-spack.git && spack repo add artdaq-spack
cd artdaq-spack
git reset --hard e7f4a8998b0497ee6831a0ddb1e7ae0a487031bb

cd ${OTSDAQ_HOME}
spack add otsdaq-suite@v2_08_00 artdaq=31207 s=126
spack concretize -f
spack install -j`nproc`
spack install -j1

###################################################
#Installing Tracker and Burninbox packages
###################################################
cd ${OTSDAQ_HOME}
cd spack/repos
git clone https://gitlab.cern.ch/otsdaq/spack/otsdaq-cms-burninbox-spack.git && spack repo add otsdaq-cms-burninbox-spack
git clone https://gitlab.cern.ch/otsdaq/spack/otsdaq-cms-tracker-spack.git && spack repo add otsdaq-cms-tracker-spack

cd ${OTSDAQ_HOME}
spack add otsdaq-cmsburninbox
spack add otsdaq-cmstracker
spack concretize -f

spack develop otsdaq-cmsburninbox@latest
spack develop otsdaq-cmstracker@latest

spack install -j`nproc`
spack concretize -f

###################################################
#Installing User data repository
###################################################
cd ${OTSDAQ_HOME}
git clone https://gitlab.cern.ch/otsdaq/otsdaq-cms-burninbox-data.git


###################################################
#Get the setup file
###################################################
cd ${OTSDAQ_HOME}
cp otsdaq-cms-burninbox-data/SetupFiles/DevelBurninBoxSetup.sh .
source DevelBurninBoxSetup.sh
