//NimPlus_app JS
//Created by Ben Hawks (bhawks at fnal.gov)
/* 
TODO

 - (Later) Have the ability to load previous versions of config? 

 --- Save Procedure ---
 set subsetBasePath value
 -> getSubsetRecords
	-> getFieldsOfRecords
	   -> setFieldValuesForRecords
	  -> popUpSaveModifiedTablesForm
		-> activateGroup
	  -> getFieldValuesForRecords

	  
	  
*/



var _TABLE_BOOL_TYPE_TRUE_COLOR = "rgb(201, 255, 201)";
var _TABLE_BOOL_TYPE_FALSE_COLOR = "rgb(255, 178, 178)";
var gSelectedInterfaceUID = null;
var timeout = null; //voltField timeout value, used to wait till user done entering a value	
var stepTimeout = null;
var trigTimeout = null; //triggerSyncWordCalc timeout val, used to wait till user done entering a value
var modifiedList = []; //List of values that are modified, used to keep track of what values need to be updated
var invalidInput = false; //Track if any textbox input is invalid, used to prevent saving if there is
var loadMuxArr = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
var syncArray = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] //empty array for 40Mhz Sync Word, 24 bits
var inputMuxArray = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] //Empty Input Mux Array, 4 vals per fw block, current max of 4 fw blocks/Physical NIM+ Unit
var outputMuxArray = [0, 0, 0, 0, 0, 0, 0, 0] //Empty Output Mux Array, 4 vals per NimPlus Board, current max of 2 NimPlus Boards/Physical NIM+ Unit
var accelMaskArray = [0, 0, 0, 0, 0, 0, 0, 0] //empty array for Accel Clock Sync Word, 8 bits, only 1 true max
var tMuxAArray = [[0, 0, 0, 0, 0, 0, 0, 0, 0]]; //empty array for Trigger Mux Bank A
var tMuxBArray = [[0, 0, 0, 0, 0, 0, 0, 0, 0]]; //empty array for Trigger Mux Bank B
var logicArray = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0] //empty array for coincidence logic word
var gInterfaceConfigurationTable = "FECMSBurninBoxInterfaceTable";
var _modifiedTables = [];
var writeFEInterfaceStatus = false;
var FEInterfaceStatusValue = "";
var ogBoardState = "";
var tablesToHtmlMap = { 
	Status             : "Status",
	InterfaceIPAddress : "InterfaceIPAddress",
	InterfacePort      : "InterfacePort",
 	HostIPAddress      : "HostIPAddress",
    HostPort           : "HostPort",
 	DataPublisherPort  : "DataPublisherPort",
	CommentDescription : "Comment"
}

// ------------------------------ JQuery UI Stuff ------------------------------

$(document).ready(function () {
	// Set Background color		
	//#6b86ff = Light Blue, indicates page is unchanged/ @ default state
	$("body").css("background-color", "#6b86ff")

	// CSS for Inner (hidden) divs
	//Hide the divs by default

	//ConnCtl
	$("#ConnCtl").show();

	//InChEnCtl
	// $("#InChEnCtl").hide();

	// //BoardCfgCtl
	// $("#BoardCfgCtl").hide();

	// //DacCtl
	// $("#DacCtl").hide();

	// //DWConfig
	// $("#DWConfigCtl").hide();

	// //InMuxCtl
	// $("#InMuxCtl").hide();

	// //OutMuxCtl
	// $("#OutMuxCtl").hide();
	// /*
	// //ResetCtl
	// $("#ResetCtl").hide();
				
	// //EnableCtl
	// $("#EnableCtl").hide();
	// */
	// //SyncCtl
	// $("#SyncCtl").hide();

	// //VetoCtl
	// $("#VetoCtl").hide();

	// //PsCtl
	// $("#PsCtl").hide();

	// //SelLogCtl
	// $("#SelLogCtl").hide();

	// //BkpCtl
	// $("#BkpCtl").hide();

	// //SigGenCtl
	// $("#SigGenCtl").hide();


	// //BurstCtl
	// $("#BurstCtl").hide();



	// //Setup button actions to hide/show the divs
	$("#ConnCtlBtn").click(function () {
		$("#ConnCtl").toggle(400);
	});

	// $("#InChEnCtlBtn").click(function () {
	// 	$("#InChEnCtl").toggle(400);
	// });

	// $("#BoardCfgCtlBtn").click(function () {
	// 	$("#BoardCfgCtl").toggle(400);
	// });

	// $("#DacCtlBtn").click(function () {
	// 	$("#DacCtl").toggle(400);
	// });

	// $("#DWConfigBtn").click(function () {
	// 	$("#DWConfigCtl").toggle(400);
	// });

	// $("#InMuxCtlBtn").click(function () {
	// 	$("#InMuxCtl").toggle(400);
	// });

	// $("#OutMuxCtlBtn").click(function () {
	// 	$("#OutMuxCtl").toggle(400);
	// });
	// /*    
	// $("#ResetCtlBtn").click(function(){
	// 	$("#ResetCtl").toggle(400);
	// });
		  
	// $("#EnableCtlBtn").click(function(){
	// 	$("#EnableCtl").toggle(400);
	// });  
	// 				*/
	// $("#SyncCtlBtn").click(function () {
	// 	$("#SyncCtl").toggle(400);
	// });

	// $("#VetoCtlBtn").click(function () {
	// 	$("#VetoCtl").toggle(400);
	// });

	// $("#PsCtlBtn").click(function () {
	// 	$("#PsCtl").toggle(400);
	// });

	// $("#SelLogCtlBtn").click(function () {
	// 	$("#SelLogCtl").toggle(400);
	// });

	// $("#BkpCtlBtn").click(function () {
	// 	$("#BkpCtl").toggle(400);
	// });

	// $("#SigGenCtlBtn").click(function () {
	// 	$("#SigGenCtl").toggle(400);
	// });


	// $("#BurstCtlBtn").click(function () {
	// 	$("#BurstCtl").toggle(400);
	// });


	// //Setup IP Address Mask for input
	// $('.ip_address').mask('0ZZ.0ZZ.0ZZ.0ZZ', { translation: { 'Z': { pattern: /[0-9]/, optional: true } } });

	// //Setup Hex/Dec mask for triggerSyncWord input
	// $('#triggerSyncWord').mask('ZZHHHHHHL', {
	// 	'translation': {
	// 		Z: { pattern: /[0x]/, optional: true },
	// 		H: { pattern: /[A-Fa-f0-9]/ },
	// 		L: { pattern: /[0-9]/ }
	// 	}
	// });

	// //Setup trigger sync word as a spinner
	// var spinner = $("#triggerSyncWord").spinner({
	// 	min: 0,
	// 	max: 1777215
	// });
	// $("#triggerSyncWord").on("spinstop", function () {
	// 	triggerSyncWordCalc(this.value);
	// 	addModifiedList('AcceleratorClockMask', this.value);
	// });
	//console.log(spinner);
	//console.log("jquery ui setup done!")

	//Grab values to populate fields
	console.log("Getting values from: " + gInterfaceConfigurationTable);
	
	ConfigurationAPI.getSubsetRecords(
		gInterfaceConfigurationTable /*subsetBasePath*/,
		"", //"FEInterfacePluginName=FEOtsUDPTemplateInterface"/*filterList*/,
		getInterfaceUIDsForList /*responseHandler*/,
		_modifiedTables /*modifiedTables*/)
});

// ---------------------------------------------- Field Population ------------------------------------------------------

var bitCnt;

//Gets the interface UIDs and update the form data with the values from the first UID
function getInterfaceUIDsForList(interfaceUIDs) {
	if (interfaceUIDs.length == 0) {
		Debug.log("No Interface UID's found", Debug.HIGH_PRIORITY);
		return;
	}
	else {
		console.log(interfaceUIDs);
		$.each(interfaceUIDs, 
			function (val, text) 
			{
				console.log(val, text)
				$('#BurninBoxInterfaceSelectUID').append($('<option></option>').val(text).html(text))
			}
		);
		getInterfaceValues(interfaceUIDs[0]);
	}
}

//Update form data for selected interface UID
function getInterfaceValues(selectedInterfaceUID) {
	gSelectedInterfaceUID = [selectedInterfaceUID];
	console.log(gSelectedInterfaceUID);
	
	ConfigurationAPI.getFieldsOfRecords(
		gInterfaceConfigurationTable, gSelectedInterfaceUID,//["FEBurninBoxInterface"]
		"",/*_editableFieldList*/
		-1,//maxDepth - This table doesn't link to any other yet
		getInterfaceFieldsForForm,//responseHandler
		_modifiedTables//modifiedTables
	);
};

function getInterfaceFieldsForForm(recFields) {
	//console.log("recFields:  \n");
	console.log(recFields);
	ConfigurationAPI.getFieldValuesForRecords(
		gInterfaceConfigurationTable, gSelectedInterfaceUID,
		recFields,
		getInterfaceValuesForForm,
		_modifiedTables)
}

function getCycleValues(selectedUID) {
	ConfigurationAPI.getSubsetRecords(
		"BurninBoxCycleTable" /*subsetBasePath*/,
		"", //"FEInterfacePluginName=FEOtsUDPTemplateInterface"/*filterList*/,
		getCycleFieldsForForm /*responseHandler*/,
		_modifiedTables /*modifiedTables*/
	);
};

function getCycleFieldsForForm(recFields) {
	//Debug.log("recFields found = " + recFields.length);	
	//console.log("getInterfaceValuesForForm input: \n");
	console.log(recFields);
	gCycleRows = recFields
	ConfigurationAPI.getFieldsOfRecords(
		"BurninBoxCycleTable", recFields[0],//["FEBurninBoxInterface"]
		"",/*_editableFieldList*/
		-1,//maxDepth - This table doesn't link to any other yet
		getCycleValuesForForm,//responseHandler
		_modifiedTables//modifiedTables
	);
}

function getCycleValuesForForm(recFields) {
	console.log(recFields);
	console.log(gCycleRows[0]);
	ConfigurationAPI.getFieldValuesForRecords(
		"BurninBoxCycleTable", gCycleRows,
		recFields,
		setCycleValuesForForm,
		_modifiedTables)
}

function setCycleValuesForForm(recFields) {
	console.log(recFields);
}
function getInterfaceValuesForForm(recFields) {
	//Debug.log("recFields found = " + recFields.length);	
	//console.log("getInterfaceValuesForForm input: \n");
	console.log(recFields);
	//console.log("\n");

	let cycleTable;
	let cycleTableGroupId;
	for (let record of recFields) {
		//console.log(record.fieldColumnName)
		if(record.fieldPath == "LinkToBurninBoxCycleTable"){
			cycleTable = record.fieldValue;
		}
		else if(record.fieldPath == "LinkToBurninBoxCycleGroupID"){
			cycleTableGroupId = record.fieldValue;
			getElementById("BurninBoxCycleGroup").value = record.fieldValue;
		}
		else{
			let htmlElement = getElementById(tablesToHtmlMap[record.fieldPath]);
			if(htmlElement){
				try {
					if (record.fieldPath == "Status") {
						if (record.fieldValue == "Yes" || record.fieldValue == "True" || record.fieldValue == "On") {
							ogBoardState = "On";
							htmlElement.value = "On";
						}
						else {
							ogBoardState = "Off";
							htmlElement.value = "Off";
						}
					}
					else {
						htmlElement.value = record.fieldValue;
					}
				}
				catch (e) {
					console.log("\n!!!_-_-_WARNING_-_-_!!!\n");
					console.log("Exception occured while trying to set an element supposedly named " + tablesToHtmlMap[record.fieldPath] + " to the value of " + record.fieldValue + " from the field " + record.fieldPath);
					console.log("Incoming exception data...\n");
					console.log(e);
					console.log("\n!!!_-_-_END WARNING_-_-_!!!\n");
				}
			}
			else {
				console.log("\nNo match for " + record.fieldPath);
				// console.log(record.fieldColumnName);
				// console.log(field[1]);
				// console.log(record.fieldColumnName == field[1]);		 
			}
			// }
		}

	}
	getCycleValues(cycleTableGroupId);

	return;
	for (let record of recFields) {
		for (let field of fieldList) {
				// console.log(field);
				// console.log(field[0]);
				// console.log(field[1]);
					// console.log(i)
			console.log(record.fieldColumnName)
			// console.log(record.fieldColumnName.includes(field[1]));
			if(record.fieldPath == "LinkToBurninBoxCycleTable" || record.fieldPath == "LinkToBurninBoxCycleGroupID"){
				continue;
			}				
			else if (record.fieldPath.includes(field[1])) { //found value
				try {
					if (getElementById(field[0]).type == "checkbox" && !record.fieldPath.includes("CoincidenceLogicWord") && !record.fieldPath.includes("TriggerClockMask") && !record.fieldPath.includes("AcceleratorClockMask")) {
						if (record.fieldValue == "Yes" || record.fieldValue == "True" || record.fieldValue == "On") {
							getElementById(field[0]).checked = true;
							//console.log("checkbox element " + i[0] + " set to checked = true")
						}
						else {
							getElementById(field[0]).checked = false;
							//console.log("checkbox element " + i[0] + " set to checked = false")
						}
					}
					else if (record.fieldPath.includes("CoincidenceLogicWord")) {
						for (bitCnt = 0; bitCnt <= 15; bitCnt++) {
							getElementById("SL" + bitCnt.toString()).checked = ((record.fieldValue >> (bitCnt)) % 2 != 0);
							logicArray[bitCnt] = ((record.fieldValue >> (15 - bitCnt)) % 2 != 0); //intentionally reversing bit order
						}
						//console.log("Coincidence Logic Word set");
					}
					else if (record.fieldPath.includes("Status")) {
						if (record.fieldValue == "Yes" || record.fieldValue == "True" || record.fieldValue == "On") {
							ogBoardState = "On";
							getElementById(field[0]).value = "On";
							//console.log("NimPlus Status " + i[0] + " set to On/Enabled")
						}
						else {
							ogBoardState = "Off";
							getElementById(field[0]).value = "Off";
							//console.log("NimPlus Status " + i[0] + " set to Off/Disabled")
						}
					}
					else {
						getElementById(field[0]).value = record.fieldValue;
						//console.log("set element " + i[0] + " to value " + a.fieldValue + " from " + a.fieldPath)
					}
				}
				catch (e) {
					console.log("\n!!!_-_-_WARNING_-_-_!!!\n");
					console.log("Exception occured while trying to set an element supposedly named " + field[0] + " to the value of " + record.fieldValue + " from the field " + record.fieldPath);
					console.log("Incoming exception data...\n");
					console.log(e);
					console.log("\n!!!_-_-_END WARNING_-_-_!!!\n");
				}
			}
			else {
				// console.log("\nno match for " + record.fieldColumnName);
				// console.log(record.fieldColumnName);
				// console.log(field[1]);
				// console.log(record.fieldColumnName == field[1]);		 
			}
		}
	}



};


//----------------------------------------- Save Functions -----------------------------------------


// Save Page Function
function savePageValues(e) {
	if (modifiedList.length > 0) {
		if (invalidInput == false) {



			//	ConfigurationAPI.getSubsetRecords(
			//      gInterfaceConfigurationTable /*subsetBasePath*/ ,
			//        "", //"FEInterfacePluginName=FEOtsUDPTemplateInterface"/*filterList*/,
			//        getNimFieldsForSave /*responseHandler*/ ,
			//        _modifiedTables /*modifiedTables*/ )      
			if (writeFEInterfaceStatus == false) { //Dont need to set FEInterface status 
				console.log("Don't need to write to FEInterfaceTable, skipping that write.");
				ConfigurationAPI.getFieldsOfRecords(
					gInterfaceConfigurationTable, selUID,//["NimPlus0"]
					"",/*_editableFieldList*/
					-1,//maxDepth
					setNimFieldValuesForSave,//responseHandler
					//_modifiedTables//modifiedTables
				)
			}
			else {
				console.log("Need to write to FEInterfaceTable...");
				ConfigurationAPI.getFieldsOfRecords( //Need to set FEInterface status
					"FEInterfaceTable", selUID,//["NimPlus0"]
					"",/*_editableFieldList*/
					-1,//maxDepth
					setFEInterfaceStatusForSave,//responseHandler
					//_modifiedTables//modifiedTables
				)
			}
		}
		else {
			document.getElementById("saveEl").innerHTML = "Warning! Unable to save, some fields contain invalid values";
			console.log("Warning! Textbox on page has invalid value, aborting save...")
			console.log("Please check the following fields for possible invalid values (some might beSigNorm/ correct)");
			Debug.log("CMS Burnin Box Configuration changes NOT saved, invalid value(s) in fields", Debug.HIGH_PRIORITY);
			console.log(modifiedList);
		}
	}
	else {
		Debug.log("Error! No Changes to Save!", Debug.WARN_PRIORITY);
	}
}


/* //Unused Function, still useful as reference
function getNimFieldsForSave(nimUids)
{
	if (nimUids.length == 0)
{
		Debug.log("No NIMPlus UID's found", Debug.HIGH_PRIORITY);
		return;
	}
	gInterfaceUIDs = nimUids;
	console.log("getNimUIDs' Input: \n")
	console.log(nimUids);
	console.log("\n");
	Debug.log("getNimUIDs ", Debug.INFO_PRIORITY);

	ConfigurationAPI.getFieldsOfRecords(
		gInterfaceConfigurationTable, nimUids,//["NimPlus0"]
		"",//_editableFieldList
		-1,//maxDepth
		setNimFieldValuesForSave,//responseHandler
		_modifiedTables//modifiedTables
	);
}
*/
function setFEInterfaceStatusForSave(recFields) {
	//console.log("setFEInterfaceStatusForSave hit");
	var valsToWrite = [];
	var objToWrite = [];
	//Debug.log("recFields found = " + recFields.length);	

	//console.log("setFEInterfaceStatusForSave input: \n");
	//console.log(recFields);
	//console.log("\n"); 
	for (let record of recFields) {
		if (record.fieldColumnName == "Status" && record.fieldRelativePath == "") {
			objToWrite.push(a); //objects to update
			valsToWrite.push(FEInterfaceStatusValue); //values for the corresponding objects
		}
		else {
			//console.log("Status field not there?!?!")
		}
	}
	ogBoardState = FEInterfaceStatusValue;
	//console.log(objToWrite);
	//console.log(valsToWrite);

	ConfigurationAPI.setFieldValuesForRecords(
		"FEInterfaceTable",//subsetBasePath 
		selUID,//recordArr
		objToWrite,//fieldObjArr
		valsToWrite,//valueArr
		FEInterfaceToNIMPlusConfigWrite,//responseHandler
		_modifiedTables//modifiedTables
	)
}

function FEInterfaceToNIMPlusConfigWrite(newModifiedTables) {
	_modifiedTables.push(newModifiedTables[0])
	//console.log("FEInterfaceToNimplus handoff hit");
	//console.log(newModifiedTables[0]);
	//console.log(_modifiedTables);
	ConfigurationAPI.getFieldsOfRecords(
		gInterfaceConfigurationTable, selUID,//["NimPlus0"]
		"",/*_editableFieldList*/
		-1,//maxDepth
		setNimFieldValuesForSave,//responseHandler
		_modifiedTables//modifiedTables 
	)
}

function setNimFieldValuesForSave(recFields) {
	var valsToWrite = [];
	var objToWrite = [];
	//Debug.log("recFields found = " + recFields.length);	

	//console.log("setNimFields input: \n");
	//console.log(recFields);
	//console.log("\n");


	//Compare Modified list and list of returned fields, populate a 2 arrays with the objects and data to write 
	//to the table of the elements of the page that have been modified since last save    
	for (let i of modifiedList) {
		for (let record of recFields) {
			if (record.fieldColumnName == i[0]) {
				objToWrite.push(a); //objects to update
				valsToWrite.push(i[1]); //values for the corresponding objects
				//console.log("Object found");
			}
			else {
				//console.log("\nno match for " + record.fieldColumnName);      
			}
		}
	}
	//console.log(objToWrite);
	//console.log(valsToWrite);

	ConfigurationAPI.setFieldValuesForRecords(
		gInterfaceConfigurationTable,//subsetBasePath 
		selUID,//recordArr
		objToWrite,//fieldObjArr
		valsToWrite,//valueArr
		saveNimTableDialog,//responseHandler
		_modifiedTables//modifiedTables
	)
};

function saveNimTableDialog(newModifiedTables) {
	modifiedList = [];
	//console.log(newModifiedTables);
	//console.log(_modifiedTables);
	_modifiedTables = newModifiedTables.splice(0);
	//console.log(_modifiedTables);   
	ConfigurationAPI.saveModifiedTables(
		_modifiedTables,
		activateNimTables
	)
};

function activateNimTables(SavedTable, SavedGroup, SavedAlias) {
	for (let i of SavedGroup) {
		ConfigurationAPI.activateGroup(i.groupName, i.groupKey, false)
	}

	//#43c130 = green color, alerts user that changes are saved
	$("body").css("background-color", "#43c130");
	//Table Updates Here					
	Debug.log("NimPlus Configuration Changes Saved to UID " + selUID[0], Debug.INFO_PRIORITY);
	console.log("Page Values Saved");
	console.log(modifiedList);
	_modifiedTables = [];
	writeFEInterfaceStatus = false;
	document.getElementById("saveEl").innerHTML = "Save Successful!";
};


//------------------------------------Error Check etc. Functions------------------------------------



//Calculate the 40 Mhz Sync Mask
function syncMaskCalc(val, a) {
	syncArray[a] = val;
	var n = 0;
	var l = syncArray.length;
	for (var i = 0; i < l; i++) {
		n |= ((syncArray[i] ? 1 : 0) << i)
		//n = (n << 1) + (syncArray[i]?1:0);
	};
	//console.log(n)
	$("#triggerSyncWord").val(n);
	return n;
}

function inputMuxCalc(val, a) {
	inputMuxArray[a] = val;
	var n = 0;
	var l = inputMuxArray.length;
	for (var i = 0; i < l; i++) {
		n |= ((inputMuxArray[i]) << i * 2)
		//n = (n << 1) + (syncArray[i]?1:0);
	};
	console.log(inputMuxArray)
	return n;
}

function outputMuxCalc(val, a) {
	outputMuxArray[a] = val;
	var n = 0;
	var l = outputMuxArray.length;
	for (var i = 0; i < l; i++) {
		n |= ((outputMuxArray[i]) << i * 2)
		//n = (n << 1) + (syncArray[i]?1:0);
	};
	console.log(outputMuxArray)
	return n;
}



//Calculate the Accelerator Clock Sync Mask
function accelMaskCalc(val, a) {
	n = (val ? 1 : 0) << a // Should only have 1 bit true ever, so dont save values like the 40Mhz mask
	//console.log(n);
	return n;
}

//Calculate the Coincidence Logic Word
function LogicWordCalc(val, a) {
	logicArray[a] = val;
	var n = 0;
	var l = logicArray.length;
	for (var i = 0; i < l; i++) {
		n = (n << 1) + (logicArray[i] ? 1 : 0);
	}
	//console.log(n)
	return n;
}


//Return an element by it's ID string
function getElementById(id) {
	e = document.getElementById(id);
	return e;
}
//Return an element's value by it's ID String
function getValueById(id) {
	e = document.getElementById(id);
	return e.value;
}

//Set invalid input flag to false to allow saving
function validInput(e) {
	invalidInput = false;
}

//Set invalid input flag to true to disallow saving
function invalidValue(e) {
	invalidInput = true;
}

//Delay&Width Error Checking
function dwValidCheck(firstVal, secondValElId, msgElId) {
	firstVal = parseInt(firstVal);
	var secondVal = parseInt(document.getElementById(secondValElId).value);
	if ((firstVal + secondVal) > 64) {
		//console.log((firstVal + secondVal));
		document.getElementById(msgElId).innerHTML = "Invald Delay/Width Combination! Sum of Delay and Width should be <=64!";
		$("#" + msgElId).css("background-color", "red");
		invalidInput = true;
	} else {
		document.getElementById(msgElId).innerHTML = "";
		$("#" + msgElId).css("background-color", "#eee");
		invalidInput = false;
	}

}

//Add a value to the list to save into the selected row (UID) of the NIMPlus table
function addModifiedList(tableVal, tableDat) {
	//#fcd125 = yellow color, notifies user that there are unsaved changes on the page
	$("body").css("background-color", "#fcd125")
	document.getElementById("saveEl").innerHTML = "Unsaved Changes are Present";

	var modifiedListNames = modifiedList.map(function (value, index) { return value[0]; });
	existsAt = null;
	existsAt = $.inArray(tableVal, modifiedListNames);
	//document.getElementById("saveEl").innerHTML = "";
	if (existsAt < 0) {
		//console.log(existsAt);
		//console.log($.inArray(tableVal, modifiedList[0]));
		//console.log(tableVal);
		//console.log(modifiedList[0]);
		//console.log([tableVal, tableDat]);
		modifiedList.push([tableVal, tableDat]);
		//console.log(modifiedList);
		//       console.log(modifiedList);
	} else {
		modifiedList[existsAt] = [tableVal, tableDat];
		//console.log(" element already exists in modified list, not updating list...")
	}

}

function FEInterfaceSetStatus(state) {
	//console.log("ogBoardState = " + ogBoardState);
	//console.log("desired board state = " + state);

	FEInterfaceStatusValue = state;
	if (state == ogBoardState)
		writeFEInterfaceStatus = false;
	else
		writeFEInterfaceStatus = true;

	//console.log("writeFEInterfaceStatus = " + writeFEInterfaceStatus)

}
