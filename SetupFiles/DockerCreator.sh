echo "Pulling image ${IMAGE}:${TAG}"
$TOOL --storage-opt ignore_chown_errors=true pull gitlab-registry.cern.ch/$ORIGIN/otsdocker/$IMAGE:$TAG
echo "Creating a new container with the name ${CONTAINER}"
echo exit | $TOOL run --privileged --net host -p 2015:2015 -p 2020:2020 -p 2021:2021 -p 2022:2022 -p 2023:2023 --name $CONTAINER -h $HOSTNAME -v $1/otsdaq-cms-burninbox-data:/otsdaq/otsdaq-cms-burninbox-data -v $2/OtsdaqData:/otsdaq/OtsdaqData -i gitlab-registry.cern.ch/${ORIGIN}/otsdocker/$IMAGE:$TAG 
$TOOL start $CONTAINER 
$TOOL ps 
$TOOL exec $CONTAINER /otsdaq/otsdaq-cms-burninbox-data/Scripts/ContainerSetup.sh 
$TOOL stop $CONTAINER
echo "Container ${CONTAINER} is ready for use!"
echo "Edit the module xml with the proper ip address"
echo "Run './LaunchTmux.sh ${CONTAINER}' to start a session"
