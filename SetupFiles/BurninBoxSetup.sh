export CONFIGURATION_NAME=Default
#export PROJECT_HOME=$(echo $PWD | sed -e 's|/user||')
export PROJECT_HOME=$PWD
export BURNINBOX_SETTINGS_HOME=${PROJECT_HOME}/otsdaq-cms-burninbox-data
export SPACK_HOME=${PROJECT_HOME}/spack
export USER_SETTINGS_HOME=${BURNINBOX_SETTINGS_HOME}/${CONFIGURATION_NAME}
export OTS_MAIN_PORT=2015

export SPACK_DISABLE_LOCAL_CONFIG=true 
source ${SPACK_HOME}/spack/share/spack/setup-env.sh

#spack compiler find
#This is for users 
spack env activate ots

#return
export MRB_SOURCE=$PROJECT_HOME

##### DATA DIRECTORY CONFIGURATION ##########
export OTSDAQ_DATA_TOP_DIR=${PROJECT_HOME}/OtsdaqData
mkdir -p ${OTSDAQ_DATA_TOP_DIR}
#OTSDAQ_DATA is the dirctory where all histograms are stored!!
export OTSDAQ_DATA=${OTSDAQ_DATA_TOP_DIR}/BurninBoxOtsdaqData
mkdir -p ${OTSDAQ_DATA}

##### OTSDAQ MAIN ENVIRONMENT VARIABLES CONFIGURATION ##########
#USER_DATA is the directory where RunNumber, Logs and other user informations are stored
export USER_DATA=${USER_SETTINGS_HOME}/UserData
#ARTDAQ_DATABASE_URI is the variable pointing to the USER database
export ARTDAQ_DATABASE_URI=filesystemdb://${USER_SETTINGS_HOME}/Database

##### BURNIN BOX CONFIGURATION ##########
#Each center has its own specific configuration for the sensors on the controller - NCP
export BURNINBOX_CONFIGURATION_FILE=${USER_SETTINGS_HOME}/HardwareConfiguration/BurninBoxConfiguration_${CONFIGURATION_NAME}.xml
export MODULE_NAMES_FILE=${OTSDAQ_DATA_TOP_DIR}/tmp/ModuleNames.cfg

##### OUTERTRACKER CONFIGURATION ##########
export CACTUSROOT=/opt/cactus
#THIS IS USED ONLY FOR DEVELOP VERSION
#export PH2ACF_BASE_DIR=${PROJECT_HOME}/otsdaq-cmstracker/otsdaq-cmstracker/Ph2_ACF
export PH2ACF_BASE_DIR=${BURNINBOX_SETTINGS_HOME}/Ph2_ACF
#export ACFSUPERVISOR_ROOT=${OTSDAQ_CMSTRACKER_DIR}/otsdaq-cmstracker/ACFSupervisor
export OTSDAQ_RESULTS_FOLDER=${OTSDAQ_DATA_TOP_DIR}/Ph2_ACF_Results

rm -f ${OTSDAQ_UTILITIES_DIR}/WebGUI/CMSBurninBoxWebPath
ln -fs ${USER_SETTINGS_HOME}/UserWebGUI ${OTSDAQ_UTILITIES_DIR}/WebGUI/CMSBurninBoxWebPath

echo -e "setup [275]  \t Now your user data path is USER_DATA \t\t = ${USER_DATA}"
echo -e "setup [275]  \t Now your database path is ARTDAQ_DATABASE_URI \t = ${ARTDAQ_DATABASE_URI}"
echo -e "setup [275]  \t Now your output data path is OTSDAQ_DATA \t = ${OTSDAQ_DATA}"
echo
echo
echo -e "setup [275]  \t Now use 'ots --wiz' to configure otsdaq"
echo -e "setup [275]  \t Then use 'ots' to start otsdaq"
echo -e "setup [275]  \t Or use 'ots --help' for more options"
echo
echo -e "setup [275]  \t Use 'kx' to kill otsdaq processes"
echo

alias kx='ots -k'
