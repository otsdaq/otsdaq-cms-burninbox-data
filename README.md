# Otsdaq CMS BurninBox Data

This repository holds configuration files for the CMS Burnin Box software. For installation instructions, look to that project. If you want to use docker to install the Burnin Box, that can be found here: https://gitlab.cern.ch/otsdaq/otsdocker
