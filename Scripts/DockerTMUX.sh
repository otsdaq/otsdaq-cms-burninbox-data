#!/bin/sh

# Set Session Name
SESSION="BurnInBoxOutput"
SESSIONEXISTS=$(tmux list-sessions | grep $SESSION)

# Only create tmux session if it doesn't already exist
if [ "$SESSIONEXISTS" = "" ]
then
    tmux new-session -d -s $SESSION

    tmux rename-window -t 0 'ots'
    tmux send-keys -t 'ots' 'source /otsdaq/otsdaq-cms-burninbox-data/Scripts/BurninContainer.sh' C-m  

    tmux split-window -hf
    tmux send-keys -t 'ots' 'source /otsdaq/otsdaq-cms-burninbox-data/Scripts/RunControllerContainer.sh' C-m  
    #tmux send-keys -t 'ots' C-b p # 'cat /etc/os-release' C-m

    tmux split-window -vf
    tmux send-keys -t 'ots' 'source /otsdaq/otsdaq-cms-burninbox-data/Scripts/OtsContainer.sh' C-m  

fi

# Attach Session, on the Main window
tmux attach-session -t $SESSION:0
